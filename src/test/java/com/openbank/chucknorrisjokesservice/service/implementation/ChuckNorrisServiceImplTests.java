package com.openbank.chucknorrisjokesservice.service.implementation;

import com.openbank.chucknorrisjokesservice.integration.client.ChuckNorrisClient;
import com.openbank.chucknorrisjokesservice.integration.model.JokeDTO;
import com.openbank.chucknorrisjokesservice.mapper.JokeMapper;
import com.openbank.chucknorrisjokesservice.model.JokeResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class ChuckNorrisServiceImplTests {

    @Mock
    private ChuckNorrisClient chuckNorrisClient;
    @Mock
    private JokeMapper jokeMapper;
    @Mock
    private CacheManager cacheManager;
    @InjectMocks
    private ChuckNorrisServiceImpl chuckNorrisServiceImpl;

    @Test
    void test_getJoke() {
        JokeDTO jokeDTO = new JokeDTO();
        jokeDTO.setId("test-id");
        jokeDTO.setValue("test-value");

        JokeResponse jokeResponse = new JokeResponse();
        jokeResponse.setId("test-id");
        jokeResponse.setText("test-text");

        Mockito.when(chuckNorrisClient.getJokeRandom())
                .thenReturn(jokeDTO);

        Mockito.when(jokeMapper.getJokeResponseFromJokeDTO(jokeDTO))
                .thenReturn(jokeResponse);

        Assertions.assertEquals(jokeResponse, chuckNorrisServiceImpl.getJoke());
    }

    @Test
    void test_getJokeById() {
        JokeResponse jokeResponse = new JokeResponse();
        jokeResponse.setId("test-id");
        jokeResponse.setText("test-text");

        Mockito.when(cacheManager.getCache("joke"))
                .thenReturn(Mockito.mock(Cache.class));

        Mockito.when(cacheManager.getCache("joke").get("test-id", JokeResponse.class))
                .thenReturn(jokeResponse);

        Assertions.assertEquals(jokeResponse, chuckNorrisServiceImpl.getJokeById("test-id"));
    }

}

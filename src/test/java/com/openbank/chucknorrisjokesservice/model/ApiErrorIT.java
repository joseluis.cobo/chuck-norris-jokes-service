package com.openbank.chucknorrisjokesservice.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;

@ExtendWith(SpringExtension.class)
public class ApiErrorIT {

    private static File apiErrorFile;

    @BeforeAll
    static void setup(@Autowired ResourceLoader resourceLoader) throws IOException {
        apiErrorFile = resourceLoader.getResource("classpath:api-error.json").getFile();
    }

    @Test
    void test_apiError_model_from_json_file() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ApiError apiError = objectMapper.readValue(apiErrorFile, ApiError.class);

        Assertions.assertAll(
                () -> Assertions.assertEquals("test-code", apiError.getCode()),
                () -> Assertions.assertEquals("test-description", apiError.getDescription()),
                () -> Assertions.assertEquals("test-level", apiError.getLevel()),
                () -> Assertions.assertEquals("test-message", apiError.getMessage())
        );
    }

}

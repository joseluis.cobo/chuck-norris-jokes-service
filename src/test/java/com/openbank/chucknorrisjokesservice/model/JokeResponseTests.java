package com.openbank.chucknorrisjokesservice.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class JokeResponseTests {

    @Test
    void test_joke_model() {
        JokeResponse joke = new JokeResponse();
        joke.setId("vtGhArEoS7q4DDzXlicPlw");
        joke.setText("Chuck Norris can exhale a huge plume of cigar smoke at any time.");

        Assertions.assertAll(
                () -> Assertions.assertEquals("vtGhArEoS7q4DDzXlicPlw", joke.getId()),
                () -> Assertions.assertEquals("Chuck Norris can exhale a huge plume of cigar smoke at any time.", joke.getText())
        );
    }

}

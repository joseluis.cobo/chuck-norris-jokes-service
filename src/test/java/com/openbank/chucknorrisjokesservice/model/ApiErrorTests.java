package com.openbank.chucknorrisjokesservice.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class ApiErrorTests {

    @Test
    void test_builder() {
        ApiError apiError = ApiError.builder()
                .code("test-code")
                .description("test-description")
                .level("test-level")
                .message("test-message")
                .build();

        Assertions.assertAll(
                () -> Assertions.assertEquals("test-code", apiError.getCode()),
                () -> Assertions.assertEquals("test-description", apiError.getDescription()),
                () -> Assertions.assertEquals("test-level", apiError.getLevel()),
                () -> Assertions.assertEquals("test-message", apiError.getMessage())
        );
    }

}

package com.openbank.chucknorrisjokesservice.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class JokeResponseIT {

    private static File jokeFile;

    @BeforeAll
    static void setup(@Autowired ResourceLoader resourceLoader) throws IOException {
        jokeFile = resourceLoader.getResource("classpath:joke-response.json").getFile();
    }

    @Test
    void test_joke_model_from_json_file() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JokeResponse joke = objectMapper.readValue(jokeFile, JokeResponse.class);

        // Check if ignore properties is working with a string model included.
        Assertions.assertAll(
                () -> Assertions.assertNotNull(joke),
                () -> Assertions.assertEquals("test-id", joke.getId()),
                () -> Assertions.assertEquals("test-text", joke.getText())
        );
    }

}

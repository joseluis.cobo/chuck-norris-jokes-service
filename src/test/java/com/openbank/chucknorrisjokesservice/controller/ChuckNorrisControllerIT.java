package com.openbank.chucknorrisjokesservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openbank.chucknorrisjokesservice.model.ApiError;
import com.openbank.chucknorrisjokesservice.model.JokeResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource("classpath:application-test.properties")
public class ChuckNorrisControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void test_getJoke() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/v1/joke-request"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andReturn();

        // Check that the object returned is valid.
        Assertions.assertDoesNotThrow(() -> objectMapper.readValue(result.getResponse().getContentAsString(), JokeResponse.class));
    }

    @Test
    void test_getJokeById_return_ApiError() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/v1/joke/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andReturn();

        // Check that the object returned is valid.
        Assertions.assertDoesNotThrow(() -> objectMapper.readValue(result.getResponse().getContentAsString(), ApiError.class));
    }

    @Test
    void test_getJokeById_return_JokeResponse() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/v1/joke-request"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andReturn();

        JokeResponse jokeResponse = objectMapper.readValue(result.getResponse().getContentAsString(), JokeResponse.class);

        MvcResult resultCached = mockMvc.perform(MockMvcRequestBuilders.get("/v1/joke/{id}", jokeResponse.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andReturn();

        JokeResponse jokeResponseCached = objectMapper.readValue(resultCached.getResponse().getContentAsString(), JokeResponse.class);

        // Check that the object returned is the same.
        Assertions.assertAll(
                () -> Assertions.assertEquals(jokeResponse.getId(), jokeResponseCached.getId()),
                () -> Assertions.assertEquals(jokeResponse.getText(), jokeResponseCached.getText())
        );
    }

}

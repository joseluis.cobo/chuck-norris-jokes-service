package com.openbank.chucknorrisjokesservice.controller;

import com.openbank.chucknorrisjokesservice.model.JokeResponse;
import com.openbank.chucknorrisjokesservice.service.ChuckNorrisService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class ChuckNorrisControllerTests {

    @Mock
    private ChuckNorrisService chuckNorrisService;
    @InjectMocks
    private ChuckNorrisController chuckNorrisController;

    @Test
    void test_getJoke() {
        JokeResponse jokeResponse = new JokeResponse();
        jokeResponse.setId("test-id");
        jokeResponse.setText("test-value");

        Mockito.when(chuckNorrisService.getJoke())
                .thenReturn(jokeResponse);

        JokeResponse jokeResponseCopy = chuckNorrisController.getJoke();

        Assertions.assertAll(
                () -> Assertions.assertEquals("test-id", jokeResponseCopy.getId()),
                () -> Assertions.assertEquals("test-value", jokeResponseCopy.getText())
        );
    }

    @Test
    void test_getJokeById() {
        JokeResponse jokeResponse = new JokeResponse();
        jokeResponse.setId("test-id");
        jokeResponse.setText("test-value");

        Mockito.when(chuckNorrisService.getJoke())
                .thenReturn(jokeResponse);;

        Assertions.assertAll(
                () -> Assertions.assertEquals(jokeResponse, chuckNorrisController.getJoke())
        );
    }

}

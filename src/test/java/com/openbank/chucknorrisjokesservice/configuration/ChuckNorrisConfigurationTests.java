package com.openbank.chucknorrisjokesservice.configuration;

import com.openbank.chucknorrisjokesservice.integration.properties.VendorProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

@ExtendWith(SpringExtension.class)
public class ChuckNorrisConfigurationTests {

    @Mock
    private VendorProperties vendorProperties;
    @InjectMocks
    private ChuckNorrisConfiguration configuration;

    @Test
    void test_getRestTemplate_return_same_instance() {
        Mockito.when(vendorProperties.getUri())
                .thenReturn("localhost");

        Mockito.when(vendorProperties.getBasepath())
                .thenReturn("/basepath");

        RestTemplate restTemplate = configuration.getRestTemplate();
        RestTemplate restSameInstance = configuration.getRestTemplate();

        // because it is not being registered in dependency injection container.
        Assertions.assertNotSame(restTemplate, restSameInstance);
    }
}

package com.openbank.chucknorrisjokesservice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ApplicationIT {

	@Autowired
	private Application application;

	@Test
	void contextLoads() {
		Assertions.assertNotNull(application);
	}

}

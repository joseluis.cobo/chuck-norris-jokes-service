package com.openbank.chucknorrisjokesservice.mapper;

import com.openbank.chucknorrisjokesservice.integration.model.JokeDTO;
import com.openbank.chucknorrisjokesservice.model.JokeResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {JokeMapperImpl.class})
public class JokeMapperIT {

    @Autowired
    private JokeMapper jokeMapper;

    @Test
    void test_getJokeResponseFromJokeDTO() {
        JokeDTO joke = new JokeDTO();
        joke.setIconUrl("https://assets.chucknorris.host/img/avatar/chuck-norris.png");
        joke.setId("vtGhArEoS7q4DDzXlicPlw");
        joke.setUrl("https://api.chucknorris.io/jokes/vtGhArEoS7q4DDzXlicPlw");
        joke.setValue("Chuck Norris can exhale a huge plume of cigar smoke at any time.");

        JokeResponse jokeResponse = jokeMapper.getJokeResponseFromJokeDTO(joke);

        Assertions.assertAll(
                () -> Assertions.assertEquals("vtGhArEoS7q4DDzXlicPlw", jokeResponse.getId()),
                () -> Assertions.assertEquals("Chuck Norris can exhale a huge plume of cigar smoke at any time.", jokeResponse.getText())
        );
    }

}

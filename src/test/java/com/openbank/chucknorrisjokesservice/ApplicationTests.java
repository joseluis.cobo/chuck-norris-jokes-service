package com.openbank.chucknorrisjokesservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class ApplicationTests {

    @Test
    void test_main() {
        Application.main(new String[]{});
    }

}

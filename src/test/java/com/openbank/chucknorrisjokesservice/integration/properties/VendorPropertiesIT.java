package com.openbank.chucknorrisjokesservice.integration.properties;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties(value = VendorProperties.class)
@TestPropertySource("classpath:application-test.properties")
public class VendorPropertiesIT {

    @Autowired
    private VendorProperties vendorProperties;

    @Test
    void test_values_are_loaded() {
        Assertions.assertEquals("https://api.chucknorris.io", vendorProperties.getUri());
        Assertions.assertEquals("/jokes", vendorProperties.getBasepath());
        Assertions.assertEquals("/random", vendorProperties.getRandompath());
    }

}

package com.openbank.chucknorrisjokesservice.integration.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class JokeIT {

    private static File jokeFile;

    @BeforeAll
    static void setup(@Autowired ResourceLoader resourceLoader) throws IOException {
        jokeFile = resourceLoader.getResource("classpath:joke-dto.json").getFile();
    }

    @Test
    void test_joke_model_from_json_file() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JokeDTO joke = objectMapper.readValue(jokeFile, JokeDTO.class);

        // Check if ignore properties is working with a string model included.
        Assertions.assertAll(
                () -> Assertions.assertNotNull(joke),
                () -> Assertions.assertNull(joke.getIconUrl()),
                () -> Assertions.assertEquals("test-id", joke.getId()),
                () -> Assertions.assertNull(joke.getUrl()),
                () -> Assertions.assertEquals("test-value", joke.getValue())
        );
    }

}

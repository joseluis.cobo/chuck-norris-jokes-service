package com.openbank.chucknorrisjokesservice.integration.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class JokeTests {

    @Test
    void test_joke_model() {
        JokeDTO joke = new JokeDTO();
        joke.setIconUrl("https://assets.chucknorris.host/img/avatar/chuck-norris.png");
        joke.setId("vtGhArEoS7q4DDzXlicPlw");
        joke.setUrl("https://api.chucknorris.io/jokes/vtGhArEoS7q4DDzXlicPlw");
        joke.setValue("Chuck Norris can exhale a huge plume of cigar smoke at any time.");

        Assertions.assertAll(
                () -> Assertions.assertEquals("https://assets.chucknorris.host/img/avatar/chuck-norris.png", joke.getIconUrl()),
                () -> Assertions.assertEquals("vtGhArEoS7q4DDzXlicPlw", joke.getId()),
                () -> Assertions.assertEquals("https://api.chucknorris.io/jokes/vtGhArEoS7q4DDzXlicPlw", joke.getUrl()),
                () -> Assertions.assertEquals("Chuck Norris can exhale a huge plume of cigar smoke at any time.", joke.getValue())
        );
    }

}

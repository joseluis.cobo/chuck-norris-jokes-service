package com.openbank.chucknorrisjokesservice.integration.client;

import com.openbank.chucknorrisjokesservice.integration.model.JokeDTO;
import com.openbank.chucknorrisjokesservice.integration.properties.VendorProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

@ExtendWith(SpringExtension.class)
public class ChuckNorrisClientTests {

    @Mock
    private VendorProperties vendorProperties;
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private ChuckNorrisClient chuckNorrisClient;

    @Test
    void test_getJokeRandom() {
        JokeDTO jokeDTO = new JokeDTO();
        jokeDTO.setId("test-id");
        jokeDTO.setValue("test-value");

        Mockito.when(vendorProperties.getRandompath())
                .thenReturn("/random");

        Mockito.when(restTemplate.getForObject("/random", JokeDTO.class))
                .thenReturn(jokeDTO);

        JokeDTO jokeResponse = chuckNorrisClient.getJokeRandom();

        Assertions.assertAll(
                () -> Assertions.assertEquals("test-id", jokeResponse.getId()),
                () -> Assertions.assertEquals("test-value", jokeResponse.getValue())
        );
    }

}

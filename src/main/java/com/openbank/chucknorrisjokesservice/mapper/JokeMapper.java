package com.openbank.chucknorrisjokesservice.mapper;

import com.openbank.chucknorrisjokesservice.integration.model.JokeDTO;
import com.openbank.chucknorrisjokesservice.model.JokeResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface JokeMapper {

    @Mapping(target = "id")
    @Mapping(target = "text", source = "value")
    JokeResponse getJokeResponseFromJokeDTO(JokeDTO jokeDTO);
}

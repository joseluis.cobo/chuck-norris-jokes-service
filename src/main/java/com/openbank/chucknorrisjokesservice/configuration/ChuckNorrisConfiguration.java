package com.openbank.chucknorrisjokesservice.configuration;

import com.openbank.chucknorrisjokesservice.integration.properties.VendorProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@org.springframework.context.annotation.Configuration
public class ChuckNorrisConfiguration {

    private final VendorProperties vendorProperties;

    @Autowired
    public ChuckNorrisConfiguration(VendorProperties vendorProperties) {
        this.vendorProperties = vendorProperties;
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplateBuilder()
                .rootUri(vendorProperties.getUri() + vendorProperties.getBasepath())
                .build();
    }

}

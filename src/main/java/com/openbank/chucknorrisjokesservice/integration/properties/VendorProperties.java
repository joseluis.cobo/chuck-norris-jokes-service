package com.openbank.chucknorrisjokesservice.integration.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "chuck.norris.api")
@Getter
@Setter
public class VendorProperties {

    private String uri;
    private String basepath;
    private String randompath;

}

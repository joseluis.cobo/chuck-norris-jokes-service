package com.openbank.chucknorrisjokesservice.integration.client;

import com.openbank.chucknorrisjokesservice.integration.model.JokeDTO;
import com.openbank.chucknorrisjokesservice.integration.properties.VendorProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ChuckNorrisClient {

    private final VendorProperties vendorProperties;
    private final RestTemplate restTemplate;

    @Autowired
    public ChuckNorrisClient(VendorProperties vendorProperties,
                             RestTemplate restTemplate) {
        this.vendorProperties = vendorProperties;
        this.restTemplate = restTemplate;
    }

    public JokeDTO getJokeRandom() {
        return this.restTemplate.getForObject(vendorProperties.getRandompath(), JokeDTO.class);
    }

}

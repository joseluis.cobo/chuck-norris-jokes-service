package com.openbank.chucknorrisjokesservice.integration.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(value = {"icon_url", "url"})
public class JokeDTO {

    @JsonProperty("icon_url")
    private String iconUrl;
    private String id;
    private String url;
    private String value;

}

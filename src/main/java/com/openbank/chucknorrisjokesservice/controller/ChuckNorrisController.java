package com.openbank.chucknorrisjokesservice.controller;

import com.openbank.chucknorrisjokesservice.model.JokeResponse;
import com.openbank.chucknorrisjokesservice.service.ChuckNorrisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1")
public class ChuckNorrisController {

    private final ChuckNorrisService chuckNorrisService;

    @Autowired
    public ChuckNorrisController(ChuckNorrisService chuckNorrisService) {
        this.chuckNorrisService = chuckNorrisService;
    }

    @GetMapping("/joke-request")
    @ResponseStatus(HttpStatus.OK)
    public JokeResponse getJoke() {
        return chuckNorrisService.getJoke();
    }

    @GetMapping("/joke/{id}")
    @ResponseStatus(HttpStatus.OK)
    public JokeResponse getJokeById(@PathVariable String id) {
        return chuckNorrisService.getJokeById(id);
    }

}

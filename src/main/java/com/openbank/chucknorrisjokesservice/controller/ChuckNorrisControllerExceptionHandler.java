package com.openbank.chucknorrisjokesservice.controller;

import com.openbank.chucknorrisjokesservice.exception.NotFoundException;
import com.openbank.chucknorrisjokesservice.model.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ChuckNorrisControllerExceptionHandler {

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ApiError notFoundExceptionHandler(NotFoundException exception) {
        String[] error = exception.getMessage().split(",");

        return ApiError.builder()
                .code(error[0])
                .message(error[1])
                .level(error[2])
                .description(error[3])
                .build();
    }

}

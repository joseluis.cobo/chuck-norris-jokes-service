package com.openbank.chucknorrisjokesservice.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException() {
        super("404,Not Found,error,Object has not been found.");
    }
}

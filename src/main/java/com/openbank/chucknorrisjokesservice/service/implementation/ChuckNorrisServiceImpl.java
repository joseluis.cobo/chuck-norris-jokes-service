package com.openbank.chucknorrisjokesservice.service.implementation;

import com.openbank.chucknorrisjokesservice.exception.NotFoundException;
import com.openbank.chucknorrisjokesservice.integration.client.ChuckNorrisClient;
import com.openbank.chucknorrisjokesservice.mapper.JokeMapper;
import com.openbank.chucknorrisjokesservice.model.JokeResponse;
import com.openbank.chucknorrisjokesservice.service.ChuckNorrisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

@Service
public class ChuckNorrisServiceImpl implements ChuckNorrisService {

    private final ChuckNorrisClient chuckNorrisClient;
    private final JokeMapper jokeMapper;
    private final CacheManager cacheManager;

    @Autowired
    public ChuckNorrisServiceImpl(ChuckNorrisClient chuckNorrisClient,
                                  JokeMapper jokeMapper,
                                  CacheManager cacheManager) {
        this.chuckNorrisClient = chuckNorrisClient;
        this.jokeMapper = jokeMapper;
        this.cacheManager = cacheManager;
    }

    @Override
    @CachePut(cacheNames = "joke", key = "#result.id")
    public JokeResponse getJoke() {
        return jokeMapper.getJokeResponseFromJokeDTO(chuckNorrisClient.getJokeRandom());
    }

    @Override
    public JokeResponse getJokeById(String id) throws NullPointerException {
        JokeResponse jokeResponse = cacheManager.getCache("joke").get(id, JokeResponse.class);

        if(jokeResponse == null) {
            throw new NotFoundException();
        }

        return jokeResponse;
    }
}

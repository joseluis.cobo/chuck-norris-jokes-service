package com.openbank.chucknorrisjokesservice.service;

import com.openbank.chucknorrisjokesservice.model.JokeResponse;

public interface ChuckNorrisService {

    JokeResponse getJoke();
    JokeResponse getJokeById(String id);

}

package com.openbank.chucknorrisjokesservice.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ApiError {

    private String code;
    private String message;
    private String level;
    private String description;

}

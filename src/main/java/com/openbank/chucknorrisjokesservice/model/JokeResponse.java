package com.openbank.chucknorrisjokesservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@Getter
@Setter
@RedisHash("joke")
public class JokeResponse implements Serializable {

    @Id
    private String id;
    private String text;

}

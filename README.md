# Chuck Norris Jokes Service
An awesome and atypical Chuck Norris API implementation with Spring Boot.

### What do you need?
- You must have installed [JDK 11](https://jdk.java.net/java-se-ri/11) or above in your machine. I use OpenJDK, but you can use any JDK implementation. Configure your path variable with this jdk version.
- You must have installed [Maven 3](https://maven.apache.org/install.html) or above.

### How to run
The first step is to have defined as environment variable the `REDIS_HOST`, `REDIS_PORT` and your `REDIS_PASSWORD`.

The second step is to clone the repository. Once downloaded, execute the following command in your terminal:
```shell
mvn spring-boot:run
```

If you want use the traditional way to execute the application, you can use the following commands:
```shell
mvn clean install
java -jar chuck-norris-jokes-service-<version>.jar
```

### How to run tests
Execute the following command to pass tests:
```shell
mvn clean test
```

If you want to check the coverage, you can use the goal verify to reach it:
```shell
mvn clean verify
```

And later, search the file `ndex.html` in `target/site/jacoco` and open in browser.

Enjoy!